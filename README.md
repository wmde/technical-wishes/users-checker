# Users checker

Checking if a list of users is eligible to participate in an event. This is based on https://meta.toolforge.org/accounteligibility.

# Installation
pip install -r requirements.txt

# Usage
python3 check.py
