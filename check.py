# script to read check multiple users for their accounteligibility
# use: python check.py

import urllib.parse
import urllib.request
from bs4 import BeautifulSoup

users = ["Test1", "Test2", "Test (WMDE)"]
link = "https://meta.toolforge.org/accounteligibility/57/"

passing_users = []
not_passing_users = []

for user in users:

    # get data
    response = urllib.request.urlopen(link+urllib.parse.quote(user))
    soup = BeautifulSoup(response.read(), 'html.parser', from_encoding="windows-1259")

    # check if user passes
    success = soup.findAll('div', {'class': 'success'})

    # handle results
    if (success):
        passing_users.append(user)
    else:
        not_passing_users.append(user)

print ('The following users passed:', passing_users)
print ('These ones did not pass:', not_passing_users)
